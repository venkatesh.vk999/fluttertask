import 'package:flutter/material.dart';
import 'package:flutter_task/myhomepage.dart';
import 'package:flutter_xlider/flutter_xlider.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  String title;

  MyHomePage({this.title});

  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  double mLowerValue = 0;
  double mUpperValue = 0;
  int mRadioValue = 0;

  int radioValue = 0;

  double _lowerValue = -20;
  double _upperValue = 20;

  int radioButtonValue = 0;

  String minValue = '0';
  String maxValue = '0';


  @override
  Widget build(BuildContext context) {
    String tempUnits = (mRadioValue == 0) ? '°C' : (mRadioValue == 1) ? '°F' : 'K';

    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Center(child: Container(padding: EdgeInsets.all(10),
            decoration: BoxDecoration(
                border: Border.all(
                  color: Colors.white,
                ),
                borderRadius: BorderRadius.all(Radius.circular(20))
            ),
              child: Text('Raw Food Freezer'))),
          backgroundColor: Colors.grey,
          elevation: 0,
          actions: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Icon(
                Icons.notifications,
                color: Colors.white,
              ),
            ),
          ],
        ),
        backgroundColor: Colors.grey,
        body: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.fromLTRB(15, 10, 15, 10),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: 50,
                ),
                Text(
                  'Download',
                  style: TextStyle(
                      fontSize: 25.0,
                      color: Colors.white,
                      fontWeight: FontWeight.w700),
                ),
                SizedBox(
                  height: 20.0,
                ),
                Row(
                  children: [
                    Expanded(
                      flex: 1,
                      child: Container(
                        color: Colors.lightGreen,
                        height: 50,
                        child: Center(child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            CircleAvatar(
                              backgroundColor: Colors.blue,
                              maxRadius: 15,
                              child: Icon(Icons.file_copy_rounded),
                            ),
                            SizedBox(width: 5,),
                            Text('PDF', style: TextStyle(color: Colors.white),),
                          ],
                        )),
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: Container(
                        height: 50,
                        child: Center(
                            child: Container(
                              color: Colors.white,
                              height: 50,
                              width: 2,
                            )),
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: Container(
                        color: Colors.lightGreen,
                        height: 50,
                        child: Center(child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            CircleAvatar(
                              backgroundColor: Colors.blue,
                              maxRadius: 15,
                              child: Icon(Icons.file_copy_rounded),
                            ),
                            SizedBox(width: 5,),
                            Text('Excel', style: TextStyle(color: Colors.white),),
                          ],
                        )),
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 50,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'ALERT TEMPERATURE THRESHOLD',
                      style: TextStyle(
                          fontSize: 15.0,
                          color: Colors.white,
                          fontWeight: FontWeight.w700),
                    ),
                    GestureDetector(
                        onTap: (){
                          _showPickTemperatureDialog(context);
                        },
                        child: Icon(Icons.edit, color: Colors.white,)),
                  ],
                ),
                SizedBox(height: 30,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      color: Colors.blueGrey[400],
                      height: 70,
                      width: 100,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(mLowerValue.toString()+' '+tempUnits, style: TextStyle(fontSize: 25, color: Colors.white),),
                          Text('Min', style: TextStyle(color: Colors.white),),
                        ],
                      ),
                    ),
                    SizedBox(width: 40,),
                    Container(
                      color: Colors.blueGrey[400],
                      height: 70,
                      width: 100,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(mUpperValue.toString()+' '+tempUnits, style: TextStyle(fontSize: 25, color: Colors.white),),
                          Text('Max', style: TextStyle(color: Colors.white),),
                        ],
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 30,),
                Text(
                  'DESCRIPTION',
                  style: TextStyle(
                      fontSize: 18.0,
                      color: Colors.white,
                      fontWeight: FontWeight.w700),
                ),
                SizedBox(height: 15,),
                TextFormField(
                  //controller: _emailController,
                  decoration: InputDecoration(
                    labelText: "Temperature range",
                    labelStyle: TextStyle(color: Colors.white),
                    fillColor: Colors.white,
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(0),
                      borderSide: BorderSide(
                        color: Colors.blue,
                      ),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(0.0),
                      borderSide: BorderSide(
                        color: Colors.white,
                        width: 2.0,
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 15.0,),
                GestureDetector(
                  onTap: (){
                    Navigator.push(context, MaterialPageRoute(builder: (context) => MySecondPage()));
                  },
                  child: Container(
                    color: Colors.red,
                    height: 50,
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          'Next Screen',
                          style: TextStyle(color: Colors.white, fontSize: 18, fontWeight: FontWeight.w700),
                        ),
                      ],
                    ),
                  ),
                ),

              ],
            ),
          ),
        ),
      ),
    );
  }

  void _showPickTemperatureDialog(BuildContext context) {
    showModalBottomSheet(
        context: context,
        builder: (builder) {
          return StatefulBuilder(builder: (context, setModalState) {
            return Container(
              height: MediaQuery.of(context).size.height / 1,
              color: Colors.transparent,
              //could change this to Color(0xFF737373),
              child: new Container(
                decoration: new BoxDecoration(
                    color: Colors.grey,
                    borderRadius: new BorderRadius.only()),
                child: Padding(
                  padding: EdgeInsets.fromLTRB(15, 10, 15, 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'Pick Temperature',
                            style: TextStyle(
                                fontSize: 24,
                                color: Colors.white,
                                fontWeight: FontWeight.w600),
                          ),
                          RaisedButton(
                            onPressed: () {
                              Navigator.pop(context);
                              setState((){
                                mLowerValue = _lowerValue;
                                mUpperValue = _upperValue;
                              });
                            },
                            child: Text(
                              'Set',
                              style: TextStyle(color: Colors.white),
                            ),
                            color: Colors.redAccent,
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      Row(
                        children: [
                          Expanded(
                            flex: 1,
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Radio(
                                  activeColor: Colors.white,
                                  value: 0, groupValue: radioButtonValue, onChanged: (value){
                                  setModalState(() {
                                    radioButtonValue = 0;
                                  });
                                  setState(() {
                                    mRadioValue = 0;
                                  });
                                },),
                                SizedBox(width: 10.0,),
                                Text('°C', style: TextStyle(fontWeight: FontWeight.w700, color: Colors.white, fontSize: 25),)
                              ],
                            ),
                          ),
                          Expanded(
                            flex: 1,
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Radio(
                                  activeColor: Colors.white,
                                  value: 1, groupValue: radioButtonValue, onChanged: (value){
                                  setModalState(() {
                                    radioButtonValue = 1;
                                  });
                                  setState(() {
                                    mRadioValue = 1;
                                  });
                                },),
                                SizedBox(width: 10.0,),
                                Text('°F', style: TextStyle(fontWeight: FontWeight.w700, color: Colors.white, fontSize: 25),)
                              ],
                            ),
                          ),
                          Expanded(
                            flex: 1,
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Radio(
                                  activeColor: Colors.white,
                                  value: 2, groupValue: radioButtonValue, onChanged: (value){
                                  setModalState(() {
                                    radioButtonValue = 2;
                                  });
                                  setState(() {
                                    mRadioValue = 2;
                                  });
                                },),
                                SizedBox(width: 10.0,),
                                Text('K', style: TextStyle(fontWeight: FontWeight.w700, color: Colors.white, fontSize: 25),)
                              ],
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: 20.0,),
                      FlutterSlider(
                        values: [_lowerValue, _upperValue],
                        rangeSlider: true,
                        max: 100,
                        min: -100,
                        onDragging: (handlerIndex, lowerValue, upperValue) {
                          setModalState(() {
                            _lowerValue = lowerValue;
                            _upperValue = upperValue;

                            minValue = lowerValue.toString();
                            maxValue = upperValue.toString();
                          });
                        },
                      ),
                      SizedBox(height: 20.0,),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                            color: Colors.blueGrey[400],
                            height: 70,
                            width: 100,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              mainAxisSize: MainAxisSize.min,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                (radioButtonValue == 0) ?
                                Text(minValue+' °C', style: TextStyle(fontSize: 25, color: Colors.white),) : (radioButtonValue == 1) ? Text(minValue+' °F', style: TextStyle(fontSize: 25, color: Colors.white),) : Text(minValue+' K', style: TextStyle(fontSize: 25, color: Colors.white),),
                                Text('Min', style: TextStyle(color: Colors.white),),
                              ],
                            ),
                          ),
                          SizedBox(width: 40,),
                          Container(
                            color: Colors.blueGrey[400],
                            height: 70,
                            width: 100,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              mainAxisSize: MainAxisSize.min,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                (radioButtonValue == 0) ?
                                Text(maxValue+' °C', style: TextStyle(fontSize: 25, color: Colors.white),) : (radioButtonValue == 1) ? Text(maxValue+' °F', style: TextStyle(fontSize: 25, color: Colors.white),) : Text(maxValue+' K', style: TextStyle(fontSize: 25, color: Colors.white),),
                                Text('Max', style: TextStyle(color: Colors.white),),
                              ],
                            ),
                          ),
                        ],
                      ),

                    ],
                  ),
                ),
              ),
            );
          });
        });
  }
}