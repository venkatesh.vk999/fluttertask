
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class MySecondPage extends StatefulWidget {
  String title;

  MySecondPage({this.title});

  _MySecondPageState createState() => _MySecondPageState();
}

class _MySecondPageState extends State<MySecondPage> {
  List<String> mainEmailsList = List<String>();
  List<String> invitationList = List<String>();

  String dropdownValue;
  bool _checkBox1 = false;

  int _daySelected = 0;

  bool graphValue = false;
  bool pdfValue = false;
  bool excelValue = false;

  List<String> daysList = [
    '',
    'Sunday',
    'Monday',
    'Tuesday',
    'Wednesday',
    'Thursday',
    'Friday',
    'Saturday'
  ];

  @override
  void initState() {
    super.initState();
    dropdownValue = 'Select Location';
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.grey,
          title: Text('Edit Report'),
          actions: [
            Icon(Icons.pin_drop_outlined),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: DropdownButton<String>(
                value: dropdownValue,
                icon: Icon(
                  Icons.arrow_drop_down_sharp,
                  color: Colors.white,
                ),
                iconSize: 24,
                elevation: 16,
                style: TextStyle(color: Colors.white, fontSize: 20),
                onChanged: (String newValue) {
                  setState(() {
                    dropdownValue = newValue;
                  });
                },
                items: <String>[
                  'Select Location',
                  'Hyderabad',
                  'Vijayawada',
                  'Kakinada'
                ].map<DropdownMenuItem<String>>((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Text(value, style: TextStyle(color: Colors.black),),
                  );
                }).toList(),
              ),
            ),
          ],
          elevation: 0,
        ),
        backgroundColor: Colors.grey,
        body: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Expanded(
                      flex: 1,
                      child: Checkbox(
                        value: _checkBox1,
                        onChanged: (value) {
                          setState(() {
                            _checkBox1 = value;
                          });
                        },
                        checkColor: Colors.red,
                        activeColor: Colors.white,
                      )),
                  Expanded(
                    flex: 4,
                    child: Text(
                      'Automatically email the report weekly',
                      style: TextStyle(
                          fontSize: 20.0,
                          color: Colors.white,
                          fontWeight: FontWeight.w700),
                    ),
                  ),
                ],
              ),
              (_checkBox1)
                  ? Container(
                padding:
                EdgeInsets.only(left: 20.0, top: 10.0, right: 10.0),
                constraints: BoxConstraints(minHeight: 100, maxHeight: 100),
                child: SingleChildScrollView(
                  child: Wrap(
                    crossAxisAlignment: WrapCrossAlignment.center,
                    verticalDirection: VerticalDirection.down,
                    runSpacing: 3.0,
                    spacing: 3.0,
                    children: [
                      GestureDetector(
                        onTap: () {
                          setState(() {
                            _daySelected = 1;
                          });
                        },
                        child: Card(
                          color: (_daySelected == 1)
                              ? Colors.white
                              : Colors.blueGrey,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(8.0),
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              'Sun',
                              style: TextStyle(
                                  color: (_daySelected == 1)
                                      ? Colors.black
                                      : Colors.white),
                            ),
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          setState(() {
                            _daySelected = 2;
                          });
                        },
                        child: Card(
                          color: (_daySelected == 2)
                              ? Colors.white
                              : Colors.blueGrey,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(8.0),
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text('Mon',
                                style: TextStyle(
                                    color: (_daySelected == 2)
                                        ? Colors.black
                                        : Colors.white)),
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          setState(() {
                            _daySelected = 3;
                          });
                        },
                        child: Card(
                          color: (_daySelected == 3)
                              ? Colors.white
                              : Colors.blueGrey,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(8.0),
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text('Tue',
                                style: TextStyle(
                                    color: (_daySelected == 3)
                                        ? Colors.black
                                        : Colors.white)),
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          setState(() {
                            _daySelected = 4;
                          });
                        },
                        child: Card(
                          color: (_daySelected == 4)
                              ? Colors.white
                              : Colors.blueGrey,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(8.0),
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text('Wed',
                                style: TextStyle(
                                    color: (_daySelected == 4)
                                        ? Colors.black
                                        : Colors.white)),
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          setState(() {
                            _daySelected = 5;
                          });
                        },
                        child: Card(
                          color: (_daySelected == 5)
                              ? Colors.white
                              : Colors.blueGrey,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(8.0),
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text('Thu',
                                style: TextStyle(
                                    color: (_daySelected == 5)
                                        ? Colors.black
                                        : Colors.white)),
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          setState(() {
                            _daySelected = 6;
                          });
                        },
                        child: Card(
                          color: (_daySelected == 6)
                              ? Colors.white
                              : Colors.blueGrey,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(8.0),
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text('Fri',
                                style: TextStyle(
                                    color: (_daySelected == 6)
                                        ? Colors.black
                                        : Colors.white)),
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          setState(() {
                            _daySelected = 7;
                          });
                        },
                        child: Card(
                          color: (_daySelected == 7)
                              ? Colors.white
                              : Colors.blueGrey,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(8.0),
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text('Sat',
                                style: TextStyle(
                                    color: (_daySelected == 7)
                                        ? Colors.black
                                        : Colors.white)),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              )
                  : Container(),
              SizedBox(
                height: 15.0,
              ),
              (_daySelected > 0)
                  ? Container(
                padding: EdgeInsets.only(left: 25.0, right: 10.0),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Report will be sent every ' + daysList[_daySelected],
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 16,
                          fontWeight: FontWeight.w700),
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Checkbox(
                          value: graphValue,
                          onChanged: (value) {
                            setState(() {
                              graphValue = value;
                            });
                          },
                          activeColor: Colors.white,
                          checkColor: Colors.red,
                        ),
                        SizedBox(height: 20.0),
                        Text(
                          'Graph',
                          style:
                          TextStyle(color: Colors.white, fontSize: 20),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Checkbox(
                          value: pdfValue,
                          onChanged: (value) {
                            setState(() {
                              pdfValue = value;
                            });
                          },
                          activeColor: Colors.white,
                          checkColor: Colors.red,
                        ),
                        SizedBox(height: 20.0),
                        Text(
                          'PDF',
                          style:
                          TextStyle(color: Colors.white, fontSize: 20),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Checkbox(
                          value: excelValue,
                          onChanged: (value) {
                            setState(() {
                              excelValue = value;
                            });
                          },
                          activeColor: Colors.white,
                          checkColor: Colors.red,
                        ),
                        SizedBox(height: 20.0),
                        Text(
                          'Excel',
                          style:
                          TextStyle(color: Colors.white, fontSize: 20),
                        )
                      ],
                    ),
                  ],
                ),
              )
                  : Container(),
              SizedBox(height: 15.0),
              Container(
                padding: EdgeInsets.only(left: 25.0, right: 10.0),
                child: Row(
                  children: [
                    Text(
                      'Invite People',
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 24,
                          fontWeight: FontWeight.w700),
                    ),
                    SizedBox(
                      width: 20,
                    ),
                    GestureDetector(
                      onTap: () {
                        _getInvitePeople(context);
                      },
                      child: Container(
                        height: 40,
                        width: 40,
                        decoration: new BoxDecoration(
                          shape: BoxShape.circle,
                          border: new Border.all(
                            color: Colors.white,
                            width: 2.5,
                          ),
                        ),
                        child: new Center(
                          child: Icon(
                            Icons.add,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: 20,),

              (invitationList.length > 0) ?
              Container(
                padding: EdgeInsets.only(left: 25, right: 25),
                height: 150,
                child: ListView.builder(
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  itemCount: invitationList.length,
                  itemBuilder: (context, index) {
                    return GestureDetector(
                      onTap: () {},
                      child: Card(
                        color: Colors.greenAccent,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(8.0),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            mainAxisAlignment:
                            MainAxisAlignment.spaceBetween,
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Text(
                                invitationList[index],
                                style: TextStyle(color: Colors.white),
                              ),
                              SizedBox(
                                width: 5,
                              ),
                              GestureDetector(
                                  onTap: () {
                                    setState(() {
                                      invitationList.removeAt(index);
                                    });
                                  },
                                  child: Icon(Icons.cancel)),
                            ],
                          ),
                        ),
                      ),
                    );
                  },
                ),
              ) : Container(),

              SizedBox(height: 40.0),
              GestureDetector(
                onTap: () {
                  _addExternalEmails(context);
                },
                child: Container(
                  padding: EdgeInsets.only(left: 25.0, right: 10.0),
                  child: Row(
                    children: [
                      Text(
                        'Add External Emails',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 24,
                            fontWeight: FontWeight.w700),
                      ),
                      SizedBox(
                        width: 20,
                      ),
                      Container(
                        height: 40,
                        width: 40,
                        decoration: new BoxDecoration(
                          shape: BoxShape.circle,
                          border: new Border.all(
                            color: Colors.white,
                            width: 2.5,
                          ),
                        ),
                        child: new Center(
                          child: Icon(
                            Icons.add,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: 10,
              ),

              Container(
                padding: EdgeInsets.only(left: 25, right: 25),
                height: 200,
                child: ListView.builder(
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  itemCount: mainEmailsList.length,
                  itemBuilder: (context, index) {
                    return GestureDetector(
                      onTap: () {},
                      child: Card(
                        color: Colors.greenAccent,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(8.0),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            mainAxisAlignment:
                            MainAxisAlignment.spaceBetween,
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Text(
                                mainEmailsList[index],
                                style: TextStyle(color: Colors.white),
                              ),
                              SizedBox(
                                width: 5,
                              ),
                              GestureDetector(
                                  onTap: () {
                                    setState(() {
                                      mainEmailsList.removeAt(index);
                                    });
                                  },
                                  child: Icon(Icons.cancel)),
                            ],
                          ),
                        ),
                      ),
                    );
                  },
                ),
              ),

            ],
          ),
        ),
      ),
    );
  }

  void _getInvitePeople(BuildContext context) {
    showModalBottomSheet(
        context: context,
        builder: (builder) {
          return StatefulBuilder(builder: (context, setModelState) {
            return new Container(
              height: MediaQuery.of(context).size.height / 1,
              color: Colors.transparent, //could change this to Color(0xFF737373),
              child: new Container(
                decoration: new BoxDecoration(
                    color: Colors.grey, borderRadius: new BorderRadius.only()),
                child: Padding(
                  padding: EdgeInsets.fromLTRB(15, 10, 15, 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        'Invite People',
                        style: TextStyle(
                            fontSize: 24,
                            color: Colors.white,
                            fontWeight: FontWeight.w600),
                      ),
                      SizedBox(
                        height: 5.0,
                      ),
                      TextFormField(
                        decoration: const InputDecoration(
                          icon: Icon(
                            Icons.search,
                            color: Colors.white,
                          ),
                          hintText: 'Search by name',
                          hintStyle: TextStyle(color: Colors.white),
                          border: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.white),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 20.0,
                      ),
                      Container(
                        height: 200,
                        child: ListView.builder(
                          shrinkWrap: true,
                          physics: NeverScrollableScrollPhysics(),
                          itemCount: mainEmailsList.length,
                          itemBuilder: (context, index) {
                            return GestureDetector(
                              onTap: () {

                              },
                              child: Card(
                                color: Colors.greenAccent,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(8.0),
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Row(
                                    mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                    mainAxisSize: MainAxisSize.min,
                                    children: [
                                      Text(
                                        mainEmailsList[index],
                                        style: TextStyle(color: Colors.white),
                                      ),
                                      SizedBox(
                                        width: 5,
                                      ),
                                      GestureDetector(
                                          onTap: () {
                                            setModelState(() {
                                              if(!invitationList.contains(mainEmailsList[index])) {
                                                invitationList.add(
                                                    mainEmailsList[index]);
                                              }else{
                                                invitationList.remove(mainEmailsList[index]);
                                              }
                                            });
                                            setState(() {

                                            });
                                          },
                                          child: (invitationList.contains(mainEmailsList[index])) ? Icon(Icons.remove_circle) : Icon(Icons.add_circle)),
                                    ],
                                  ),
                                ),
                              ),
                            );
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            );
          });
        });

  }

  void _addExternalEmails(BuildContext context) async {
    TextEditingController _emailController = TextEditingController();
    List<String> externalEmails = new List<String>();
    showModalBottomSheet(
        context: context,
        builder: (builder) {
          return StatefulBuilder(builder: (context, setModelState) {
            return Container(
              height: MediaQuery.of(context).size.height / 1,
              color: Colors.transparent,
              //could change this to Color(0xFF737373),
              child: new Container(
                decoration: new BoxDecoration(
                    color: Colors.grey,
                    borderRadius: new BorderRadius.only()),
                child: Padding(
                  padding: EdgeInsets.fromLTRB(15, 10, 15, 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'Add External Emails',
                            style: TextStyle(
                                fontSize: 24,
                                color: Colors.white,
                                fontWeight: FontWeight.w600),
                          ),
                          RaisedButton(
                            onPressed: () {
                              Navigator.pop(context);
                              setModelState((){
                                mainEmailsList.addAll(externalEmails);
                              });

                              setState(() {});
                            },
                            child: Text(
                              'Done',
                              style: TextStyle(color: Colors.white),
                            ),
                            color: Colors.blueGrey[400],
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      TextFormField(
                        controller: _emailController,
                        decoration: InputDecoration(
                          labelText: "Email",
                          labelStyle: TextStyle(color: Colors.white),
                          fillColor: Colors.white,
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(0),
                            borderSide: BorderSide(
                              color: Colors.blue,
                            ),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(0.0),
                            borderSide: BorderSide(
                              color: Colors.white,
                              width: 2.0,
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 20.0,
                      ),
                      (externalEmails.length > 0)
                          ? Container(
                        padding: EdgeInsets.only(
                            left: 20.0, top: 10.0, right: 10.0),
                        constraints: BoxConstraints(
                            minHeight: 140, maxHeight: 140),
                        child: SingleChildScrollView(
                          child: Wrap(
                            crossAxisAlignment: WrapCrossAlignment.center,
                            verticalDirection: VerticalDirection.down,
                            runSpacing: 3.0,
                            spacing: 3.0,
                            children: [
                              ListView.builder(
                                shrinkWrap: true,
                                physics: NeverScrollableScrollPhysics(),
                                itemCount: externalEmails.length,
                                itemBuilder: (context, index) {
                                  return GestureDetector(
                                    onTap: () {},
                                    child: Card(
                                      color: Colors.blueGrey,
                                      shape: RoundedRectangleBorder(
                                        borderRadius:
                                        BorderRadius.circular(8.0),
                                      ),
                                      child: Padding(
                                        padding:
                                        const EdgeInsets.all(8.0),
                                        child: Row(
                                          mainAxisAlignment:
                                          MainAxisAlignment
                                              .spaceBetween,
                                          mainAxisSize: MainAxisSize.min,
                                          children: [
                                            Text(
                                              externalEmails[index],
                                              style: TextStyle(
                                                  color: Colors.white),
                                            ),
                                            SizedBox(
                                              width: 5,
                                            ),
                                            GestureDetector(
                                                onTap: () {
                                                  setModelState(() {
                                                    externalEmails
                                                        .removeAt(index);
                                                  });
                                                },
                                                child:
                                                Icon(Icons.cancel)),
                                          ],
                                        ),
                                      ),
                                    ),
                                  );
                                },
                              )
                            ],
                          ),
                        ),
                      )
                          : Container(
                        child: Text(
                          'No External Emails added',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 16,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                      SizedBox(
                        height: 20.0,
                      ),
                      Container(
                        height: 70,
                        child: Row(
                          mainAxisSize: MainAxisSize.max,
                          children: [
                            Expanded(
                                child: RaisedButton(
                                  onPressed: () {
                                    if(_emailController.text.isNotEmpty) {
                                      setModelState(() {
                                        externalEmails.add(
                                            _emailController.text.trim());
                                        _emailController.clear();
                                      });
                                    }
                                  },
                                  child: Text(
                                    'Add Email',
                                  ),
                                  color: Colors.white70,
                                )),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            );
          });
        });
  }
}